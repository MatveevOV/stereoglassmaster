#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QFile>
#include <QDataStream>
#include <QDir>
#include <QSettings>
#include <QHostInfo>
#include <QUuid>
#include <QStandardItem>
#include <QElapsedTimer>

#include "clientnamesmodel.h"
#include "testresultmodel.h"
#include "datahelper.h"

class TcpServer : public QObject
{
    Q_OBJECT
public:
    explicit TcpServer(QObject *parent = nullptr);
    enum command{SEND_THEME,
                 SEND_PICTURE,
                 READY_TO_VIEW,
                 PICTURE_VIEWED,
                 TEST_END
                };

private:
    QTcpServer* m_ptcpServer;
    ClientNamesModel peerNamesModel;
    TestResultModel testResultModel;
    qint64 m_nNextBlockSize;
    QElapsedTimer pictureTimer;
    QString selectedClient;
    QString currentPicture;
    QString currentTheme;
    QMap<int, QTcpSocket*> clients;
    QMap<int, QString> clientNames;
    DataHelper dataHelper;
    bool isTimerStarted;
    void sendCommandToClient(QTcpSocket *pSocket, quint8 type, QString data = QString());
signals:
    void sendClientDisconnected();

public slots:
    virtual void slotNewConnection();
    void slotReadClient();
    ClientNamesModel *getPeerNamesModel();
    TestResultModel *getResultModel();
    void selectDevice(const QString &hostName);
    void startTest(const QString &theme);
    void sendNextPicture(QTcpSocket *pClientSocket);
    void startPictureTimer(int row);
    QVariant getThemes();
    void clientDisconnected();
    void testAborted();



};

#endif // TCPSERVER_H
