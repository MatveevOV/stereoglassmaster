#ifndef DATABASEHELPER_H
#define DATABASEHELPER_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QUuid>
#include <QDebug>
#include <QPoint>
#include <QElapsedTimer>


/* Директивы имен таблицы, полей таблицы и базы данных */
#define DATABASE_NAME               "StereoGlass"

#define USERS                       "Users"         // Название таблицы
#define USERS_ID                    "UserId"
#define USERS_LASTNAME              "LastName"
#define USERS_FIRSTNAME             "FirstName"
#define USERS_SECONDNAME            "SecondName"
#define USERS_PIC_DISTANCE          "PicDistance"

#define VIEWING_TIME                "ViewingTime"         // Название таблицы
#define VIEWING_TIME_THEME          "Theme"
#define VIEWING_TIME_USER_ID        "UserId"
#define VIEWING_TIME_PICTURE_ID     "PictureId"
#define VIEWING_TIME_TRY_NUMBER     "Try"
#define VIEWING_TIME_TIME           "Time"

class DataBaseHelper: public QObject
{
    Q_OBJECT
public:
    explicit DataBaseHelper(QObject *parent = 0);
    ~DataBaseHelper();

    void connectToDataBase();

private:
    QSqlDatabase db;
    bool openDataBase();
    bool restoreDataBase();
    void closeDataBase();
    bool createUserTable();
    bool createViewingTimeTable();
    QString m_currentUser;

public slots:
    //добавить нового пользователя или обновить существующего
    bool insertUser(const QString &lastName, const QString &firstName, const QString &secondName, const int picDistance);
    bool insertViewingTime(const QString &theme, const QString &userId,
                           const QString &pictureId, const int tryNumber, const int time);
    int getUserTry(const QString &userId, const QString &theme);
    int getUserPicDistance(const QString &userId);
    QString getCurrentUser() const;
    QVariantList getPictureData(QString userId, QString theme, QString pictureId) const;

};

#endif // DATABASEHELPER_H
