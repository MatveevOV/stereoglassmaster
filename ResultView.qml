import QtQuick 2.9
import QtQuick.Controls 2.2
import QtCharts 2.2

Item {
    property var currentUser: database.getCurrentUser()
    property var currentTheme: dataHelper.getCurrentTheme()


    Column{
        anchors.fill: parent
        spacing: 20
    ComboBox{
        id: pictureComboBox
        width: parent.width
        model: dataHelper.getAllPicureNames()
        onActivated: {
            drawChart()
        }        

    }


    ChartView {
        width: 800
        height: 500
        legend.visible: false

        LineSeries {
            id: pictureChart
            axisX: axisX
            axisY: axisY

        }

        ValueAxis {
            id: axisX
            titleText: "Номер попытки"
        }

        ValueAxis {
            id: axisY
            titleText: "Время, мс"

        }
    }
    }

    Component.onCompleted:
    {
        drawChart()
    }

function drawChart()
{
    pictureChart.removePoints(0, pictureChart.count)
    console.log(pictureComboBox.currentText)
    var picture = pictureComboBox.currentText
    var data = database.getPictureData(currentUser, currentTheme, picture);
    var minTime = 0, maxTime = 0, minTry = 1, maxTry = 1
    for (var i = 0; i <  data.length; i++)
    {
        if (data[i].x < minTry)
            minTry = data[i].x;
        if (data[i].x > maxTry)
            maxTry = data[i].x;

        if ( data[i].y < minTime)
            minTime =  data[i].y;
        if ( data[i].y > maxTime)
            maxTime =  data[i].y;
        pictureChart.append(data[i].x, data[i].y)
    }
    axisX.min = minTry
    axisX.max = maxTry
    axisY.min = minTime
    axisY.max = maxTime
    axisX.tickCount = maxTry



}


}
