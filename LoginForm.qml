import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.1

Item {

    Connections{
    target: server
    onSendClientDisconnected: {
        if (clientComboBox.count === 0)
            clientComboBox.displayText =  "Выберите устройство"
        else
            /*надо проверить с двумя клиентами*/
            clientComboBox.displayText =  currentText

    }
    }

    TextField {
        id: lastName
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        width: 360
        selectByMouse: true
        font.pixelSize: 14
        placeholderText: "Фамилия"


    }
    TextField {
        id: firstName
        anchors.top: lastName.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        width: 360
        placeholderText: "Имя"
        font.pixelSize: 14

    }
    TextField {
        id: secondName
        anchors.top: firstName.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        width: 360
        placeholderText: "Отчество"
        font.pixelSize: 14

    }
    Button {
        id: loginButton
        anchors.top: secondName.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        width: 360
        height: 40
        text: qsTr("Войти")
        onClicked: {
            checkAndPush()

        }

    }

    ComboBox {
        id: clientComboBox
        width: 360
        anchors.top: loginButton.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        model: peerNames
        textRole: "display"
        displayText: "Выберите устройство"
        onActivated: {
                displayText = currentText
            }

    }


    function checkAndPush ()
    {
        server.selectDevice(clientComboBox.currentText)
        database.insertUser(lastName.text, firstName.text, secondName.text, 0)
        stack.push(modeComponent)        

    }

}
