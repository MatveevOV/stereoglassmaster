#include "datahelper.h"


DataHelper::DataHelper(QObject *parent) : QObject(parent)
{
}

void DataHelper::setPlatfotm(bool isAndroid)
{
    this->isAndroid = isAndroid;
}

QVariant DataHelper::getFolders()
{
    QDir picturesDir;
    if (isAndroid)
        picturesDir.setPath("assets:/Pictures");
    else
        picturesDir.setPath("./Pictures");
    QStringList themes = picturesDir.entryList(QDir::AllDirs|QDir::NoDotAndDotDot);

    themes.append(BACK);

    return QVariant::fromValue(themes);
}

QString DataHelper::getCurrentTheme() const
{
    return m_currentTheme;
}


QStringList DataHelper::getAllPicureNames() const
{
    QStringList pictures;
    for (int i = 0; i < slides.length(); i++)
        pictures.append(slides.at(i).id);

    return pictures;
}

QString DataHelper::getNextPicture()
{
    currentPicture++;
    return slides.at(currentPicture).id;
}

int DataHelper::getCurrentPicture()
{
    return currentPicture;
}

bool DataHelper::hasNextPicture()
{
    return currentPicture + 1 < slides.length();
}

bool DataHelper::themeParse(QString dir)
{
    QDir themeDir(dir);
    slides.clear();
    if (themeDir.exists())
    {
        QStringList filters;
        filters << "*.jpg";
        QStringList fileList = themeDir.entryList(filters, QDir::Files, QDir::Name);
        for (int i = 0; i < fileList.length(); i+=2)
        {
            Slide slide;
            QString fileName = fileList.at(i);
            fileName.chop(6);//отрезать -L.jpg
            slide.id = fileName;
            slide.leftImagePath = QString(dir + "/" + fileName + "-L.jpg");
            slide.rightImagePath = QString(dir + "/" + fileName + "-R.jpg");
            slides.append(slide);

        }
        return true;
    }
    else
        return false;
}

void DataHelper::openTheme(QString theme)
{
    m_currentTheme = theme;
    if (isAndroid)
        themeParse("assets:/Pictures/"+ m_currentTheme);
    else
        themeParse("./Pictures/"+ m_currentTheme);
    currentPicture = -1;

}
