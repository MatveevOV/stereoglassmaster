import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {


    ListView {
        id: listview
        spacing: 1
        width: parent.width
        contentWidth: 360
        height: parent.height
        x: parent.width/2 - contentWidth/2
        y: parent.height/2
        model: server.getThemes()
        delegate: Button {
            width: 360
            height: 40
            text: modelData
            onClicked: {
                checkButton(text)

            }

        }
    }

    function checkButton(text)
    {

        //костыль: лучше использовать другое поле модели для обозначения кнопки Назад
        if (text === "Назад")
        {
            stack.pop();
        }
        else
        {
            if (mode === 0)
            {
                server.startTest(text)
                stack.push(testComponent)
            }
            else
            {
                stack.push(resultComponent)
            }
        }


    }


}


