import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {

      ListView {
        id: listView
        anchors.fill: parent
        contentWidth: headerItem.width
        flickableDirection: Flickable.HorizontalAndVerticalFlick

        header: Row {
            spacing: 1
            width: parent.width
            function itemAt(index) { return repeater.itemAt(index) }
            Repeater {
                id: repeater
                model: ["Изображение", "Время"]
                Label {
                    width: parent.width/2
                    text: modelData
                    horizontalAlignment: Text.AlignHCenter
                    font.bold: true
                    font.pixelSize: 20
                    padding: 10
                    background: Rectangle { color: "silver" }
                }
            }
        }

        model: testResult
        delegate: Column {
            id: delegate
            property int row: index
            Row {
                spacing: 1
                Rectangle
                {
                    width: listView.headerItem.itemAt(0).width
                    height: listView.headerItem.height
                    color: backgroundColor
                    Text
                    {
                        text: picture
                        font.bold: true
                        font.pixelSize: 20
                        padding: 10
                        anchors.centerIn: parent


                    }
                }
                Rectangle
                {
                    width: listView.headerItem.itemAt(1).width
                    height: listView.headerItem.height
                    color: backgroundColor
                    Text
                    {
                        id: timeText
                        text: time
                        font.bold: true
                        font.pixelSize: 20
                        padding: 10
                        visible: true
                        anchors.centerIn: parent

                    }
                    Button {
                        id: startButton
                        text: "Старт"
                        font.bold: true
                        font.pixelSize: 20
                        visible: startVisible === "true"
                        padding: 10
                        onClicked:
                        {
                            server.startPictureTimer(row);
                        }

                    }

                }






            }
            Rectangle {
                color: "silver"
                width: parent.width
                height: 1
            }
        }

        ScrollIndicator.horizontal: ScrollIndicator { }
        ScrollIndicator.vertical: ScrollIndicator { }
        onCountChanged: {listView.currentIndex = listView.count - 1}
    }

}
