#include "databasehelper.h"

DataBaseHelper::DataBaseHelper(QObject *parent) : QObject(parent)
{

}

DataBaseHelper::~DataBaseHelper()
{

}

void DataBaseHelper::connectToDataBase()
{
    if(!QFile(DATABASE_NAME).exists()){
        this->restoreDataBase();
    } else {
        this->openDataBase();
    }
}

bool DataBaseHelper::openDataBase()
{
    /* База данных открывается по заданному пути
     * и имени базы данных, если она существует
     * */
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(DATABASE_NAME);
    if(db.open()){
        return true;
    } else {
        return false;
    }
}

bool DataBaseHelper::restoreDataBase()
{
    // Если база данных открылась ...
    if(this->openDataBase()){
        // Производим восстановление базы данных
        return (this->createUserTable() && this->createViewingTimeTable()) ? true : false;
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
    return false;
}

void DataBaseHelper::closeDataBase()
{

}

bool DataBaseHelper::createUserTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE IF NOT EXISTS " USERS " ("
                            USERS_ID           " VARCHAR(255)    NOT NULL,"
                            USERS_LASTNAME     " VARCHAR(255)    NOT NULL,"
                            USERS_FIRSTNAME    " VARCHAR(255)    NOT NULL,"
                            USERS_SECONDNAME   " VARCHAR(255)    NOT NULL,"
                            USERS_PIC_DISTANCE " INTEGER    NOT NULL)"
                    )){
        qDebug() << "DataBase: error of create " << USERS;
        qDebug() << query.lastError().text();
        return false;
    } else {
        qDebug() << USERS << "created";
        return true;
    }
    return false;
}

bool DataBaseHelper::createViewingTimeTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE IF NOT EXISTS " VIEWING_TIME " ("
                    "id INTEGER PRIMARY KEY ASC, "
                            VIEWING_TIME_THEME        " VARCHAR(255)    NOT NULL,"
                            VIEWING_TIME_USER_ID      " VARCHAR(255)    NOT NULL,"
                            VIEWING_TIME_PICTURE_ID   " VARCHAR(255)    NOT NULL,"
                            VIEWING_TIME_TRY_NUMBER   " INTEGER    NOT NULL,"
                            VIEWING_TIME_TIME         " INTEGER    NOT NULL)"
                    )){
        qDebug() << "DataBase: error of create " << VIEWING_TIME;
        qDebug() << query.lastError().text();
        return false;
    } else {
        qDebug() << VIEWING_TIME << "created";
        return true;
    }
    return false;
}

QString DataBaseHelper::getCurrentUser() const
{
    return m_currentUser;
}

QVariantList DataBaseHelper::getPictureData(QString userId, QString theme, QString pictureId) const
{

    QSqlQuery query;

    QString selectPic = QString("SELECT " VIEWING_TIME_TRY_NUMBER ", " VIEWING_TIME_TIME " FROM " VIEWING_TIME
                               " WHERE " VIEWING_TIME_USER_ID " = '%1' AND "
                               VIEWING_TIME_THEME " = '%2' AND " VIEWING_TIME_PICTURE_ID " = '%3'").arg(userId)
                                                                                                   .arg(theme)
                                                                                                   .arg(pictureId);
    if (query.exec(selectPic))
    {
        QVariantList data;
        while (query.next())
        {
            int tryNumber = query.value(0).toInt();
            int time = query.value(1).toInt();
            data.append(QPoint(tryNumber, time));
        }
        return data;
    }
    else{
        qDebug() << "nothing" << query.lastError();

        return QVariantList();
    }



}

bool DataBaseHelper::insertUser(const QString &lastName, const QString &firstName, const QString &secondName, const int picDistance)
{
    QSqlQuery query;
    qDebug() << lastName << firstName << secondName << picDistance;

    QString selectId = QString("SELECT " USERS_ID " FROM " USERS " WHERE " USERS_LASTNAME " = '%1' AND "
                               USERS_FIRSTNAME " = '%2' AND " USERS_SECONDNAME " = '%3'").arg(lastName).arg(firstName).arg(secondName);



    QString userId;
    bool selectBool = query.exec(selectId);
    if(selectBool)
    {
        query.next();
        qDebug()  << "old user";
        userId = query.value(0).toString();
        QString update = QString("UPDATE " USERS " SET " USERS_PIC_DISTANCE " = %1"
                                 " WHERE " USERS_ID " = '%2'").arg(picDistance)
                                                              .arg(userId);


        query.exec(update);

    }
    if(userId.isEmpty())
    {
        userId = QUuid::createUuid().toString();        
        query.prepare("INSERT INTO " USERS " ( " USERS_ID ", "
                                                 USERS_LASTNAME ", "
                                                 USERS_FIRSTNAME ", "
                                                 USERS_SECONDNAME ", "
                                                 USERS_PIC_DISTANCE " ) "
                      "VALUES (:Id, :LastName, :FirstName, :SecondName, :PicDistance)");
        query.bindValue(":Id",userId);
        query.bindValue(":LastName", lastName);
        query.bindValue(":FirstName", firstName);
        query.bindValue(":SecondName", secondName);
        query.bindValue(":PicDistance", picDistance);

        qDebug() << "insert" << query.exec() << query.lastError();
    }
    m_currentUser = userId;
    return true;
}

bool DataBaseHelper::insertViewingTime(const QString &theme, const QString &userId, const QString &pictureId, const int tryNumber, const int time)
{
    QSqlQuery query;
    query.prepare("INSERT INTO " VIEWING_TIME " ( " VIEWING_TIME_THEME ", "
                                                    VIEWING_TIME_USER_ID ", "
                                                    VIEWING_TIME_PICTURE_ID ", "
                                                    VIEWING_TIME_TRY_NUMBER ", "
                                                    VIEWING_TIME_TIME " ) "
                  "VALUES (:Theme, :UserId, :PictureId, :Try, :Time)");
    query.bindValue(":Theme",theme);
    query.bindValue(":UserId", userId);
    query.bindValue(":PictureId", pictureId);
    query.bindValue(":Try", tryNumber);
    query.bindValue(":Time", time);
    if(!query.exec()){
        qDebug() << "error insert into " << VIEWING_TIME;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
    return false;
}

int DataBaseHelper::getUserTry(const QString &userId, const QString &theme)
{
    QSqlQuery query;   
    int tryNumber = 1;
    QString selectTry = QString("SELECT " VIEWING_TIME_TRY_NUMBER " FROM "  VIEWING_TIME " WHERE "  
                                VIEWING_TIME_USER_ID " = '%1' AND "
                                VIEWING_TIME_THEME " = '%2' ORDER BY " VIEWING_TIME_TRY_NUMBER " DESC").arg(userId)
                                                                                                       .arg(theme);

    if(query.exec(selectTry))
    {
        query.next();
        tryNumber = query.value(0).toInt() + 1;
    }


    return tryNumber;
}

int DataBaseHelper::getUserPicDistance(const QString &userId)
{
    QSqlQuery query;   
    int picDistance = 300;
    QString selectDistance = QString("SELECT " USERS_PIC_DISTANCE " FROM " USERS " WHERE "  USERS_ID " = '%1'").arg(userId);
    if(query.exec(selectDistance))
    {
        query.next();
        picDistance = query.value(0).toInt();
    }

    return picDistance;
}
