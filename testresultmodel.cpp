#include "testresultmodel.h"

TestResultModel::TestResultModel(QObject *parent)
{
    Q_UNUSED(parent);

}

void TestResultModel::addViewedPicture (const QString &pictureName, const QString &time)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_pictureNames << pictureName;
    m_pictureTimes << time;
    m_startVisbleState << "false";
    m_colors << "white";
    endInsertRows();
}

int TestResultModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_pictureNames.count();
}

QVariant TestResultModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_pictureNames.count())
        return QVariant();


    if (role == PictureRole)
        return m_pictureNames.at(index.row());
    else if (role == TimeRole)
        return m_pictureTimes.at(index.row());
    else if (role == StartVisible)
        return m_startVisbleState.at(index.row());
    else if (role == Color)
        return m_colors.at(index.row());
    return QVariant();

}

bool TestResultModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() < 0 || index.row() >= m_pictureNames.count())
        return false;


    if (role == PictureRole)
    {
        m_pictureNames.replace(index.row(), value.toString());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == TimeRole)
    {
        m_pictureTimes.replace(index.row(), value.toString());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == StartVisible)
    {
        m_startVisbleState.replace(index.row(), value.toString());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == Color)
    {
        m_colors.replace(index.row(), value.toString());
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

void TestResultModel::clearModel()
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    m_pictureNames.clear();
    m_pictureTimes.clear();
    m_startVisbleState.clear();
    m_colors.clear();
    endRemoveRows();
}

QHash<int, QByteArray> TestResultModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PictureRole] = "picture";
    roles[TimeRole] = "time";
    roles[StartVisible] = "startVisible";
    roles[Color] = "backgroundColor";
    return roles;
}
