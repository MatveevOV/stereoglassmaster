#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>
#include <QQuickItem>
#include <QDebug>

#include "databasehelper.h"
#include "datahelper.h"
#include "tcpserver.h"



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    //переменная платформа, для десктопа и андроида используются разные пути
    //к картинкам

    DataBaseHelper dbHelper;
    dbHelper.connectToDataBase();
    TcpServer server;

    engine.rootContext()->setContextProperty("database", &dbHelper);
    engine.rootContext()->setContextProperty("server", &server);
    engine.rootContext()->setContextProperty("peerNames", server.getPeerNamesModel());
    engine.rootContext()->setContextProperty("testResult", server.getResultModel());


    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
