import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3




ApplicationWindow {
    visible: true
    width: 1280
    height: 720
    title: qsTr("StereoGlass")

    property alias stack: stack
    property alias loginView: loginForm
    property alias modeComponent: modeComponent
    property alias themeComponent: themeComponent
    property alias testComponent: testComponent
    property alias resultComponent: resultComponent



    property int mode: 0

    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: stack.depth > 1
        onActivated: {
            if (stack.depth === 4 && mode === 0)
            {
                server.testAborted();
            }
            stack.pop()

        }
    }




    StackView
    {
        id: stack
        anchors.fill: parent
        initialItem:
            LoginForm {
            id:loginForm

        }


    }

    Component
    {
        id: modeComponent        
        ModeSelectForm
        {
            id: modeForm

        }
    }
    Component
    {
        id: themeComponent
        ThemeSelectForm
        {
            id: themeForm
            width: stack.width
            height: stack.height

        }
    }

    Component
    {
        id: testComponent
        TestView
        {
            id: testForm
            width: stack.width
            height: stack.height

        }
    }

    Component
    {
        id: resultComponent
        ResultView
        {
            id: resultView

        }
    }

}

