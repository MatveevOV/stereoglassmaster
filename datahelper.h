#ifndef DATAHELPER_H
#define DATAHELPER_H

#include <QObject>
#include <QDir>
#include <QDebug>
#include <QFile>
#include <QStandardPaths>
#include "databasehelper.h"

#define BACK               "Назад"


class DataHelper : public QObject
{
    Q_OBJECT
public:
    explicit DataHelper(QObject *parent = nullptr);
    void setPlatfotm(bool isAndroid);    
    int getCurrentPicture();
signals:

public slots:
    void openTheme(QString theme);
    QVariant getFolders();
    QString getCurrentTheme() const;
    QStringList getAllPicureNames() const;
    QString getNextPicture();
    bool hasNextPicture();


private:
    struct Slide
    {
       QString id;
       QString leftImagePath;
       QString rightImagePath;
    };

    QString m_currentTheme;
    int currentPicture;
    QVector<Slide> slides;

    bool themeParse(QString dir);
    bool isAndroid;


};

#endif // DATAHELPER_H
