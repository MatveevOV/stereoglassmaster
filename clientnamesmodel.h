#ifndef CLIENTNAMESMODEL_H
#define CLIENTNAMESMODEL_H

#include <QAbstractListModel>
#include <QDebug>

class ClientNamesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    ClientNamesModel(QObject *parent = 0);
    void addPeerName (const QString &peerName, const QString &uuid);
    void removePeerName (const QString &peerName);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
private:
    QStringList m_peerNames;
    QStringList m_peerUuids;

};

#endif // CLIENTNAMESMODEL_H
