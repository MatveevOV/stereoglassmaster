#include "clientnamesmodel.h"

ClientNamesModel::ClientNamesModel(QObject *parent)
{
    Q_UNUSED(parent);

}

void ClientNamesModel::addPeerName(const QString &peerName, const QString &uuid)
{
    if (!m_peerNames.contains(peerName))
    {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_peerNames << peerName;
        m_peerUuids << uuid;
        endInsertRows();
    }
}

void ClientNamesModel::removePeerName(const QString &peerName)
{
    if (m_peerNames.contains(peerName))
    {
        int index = m_peerNames.indexOf(peerName);
        qDebug() << index << peerName;
        beginRemoveRows(QModelIndex(), index, index);
        m_peerNames.removeAt(index);
        endRemoveRows();
    }
}

int ClientNamesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_peerNames.count();
}

QVariant ClientNamesModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_peerNames.count())
        return QVariant();

    if (role == Qt::DisplayRole)
        return m_peerNames.at(index.row());
    else if (role == Qt::UserRole)
        return m_peerUuids.at(index.row());
    return QVariant();
}
