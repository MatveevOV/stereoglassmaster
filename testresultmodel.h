#ifndef TESTRESULTMODEL_H
#define TESTRESULTMODEL_H

#include <QAbstractListModel>

class TestResultModel : public QAbstractListModel
{
    Q_OBJECT

public:
    TestResultModel(QObject *parent = 0);
    enum Roles {
        PictureRole = Qt::UserRole + 1,
        TimeRole,
        StartVisible,
        Color
    };
    void addViewedPicture (const QString &pictureName, const QString &time);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    void clearModel();

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QStringList m_pictureNames;
    QStringList m_pictureTimes;
    QStringList m_startVisbleState;
    QStringList m_colors;
};

#endif // TESTRESULTMODEL_H
