#include "tcpserver.h"

TcpServer::TcpServer(QObject *parent) : QObject(parent)
{
    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, 54256))
    {
        QMessageBox::critical(0,
                              "Server Error",
                              "Unable to start the server:"
                              + m_ptcpServer->errorString()
                             );
        m_ptcpServer->close();
        return;
    }
    else
    {

        QString ipAddress;
        QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
        // use the first non-localhost IPv4 address
        for (int i = 0; i < ipAddressesList.size(); ++i) {
            if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
                ipAddressesList.at(i).toIPv4Address()) {
                ipAddress = ipAddressesList.at(i).toString();
                break;
            }
        }
        // if we did not find one, use IPv4 localhost
        if (ipAddress.isEmpty())
            ipAddress = QHostAddress(QHostAddress::LocalHost).toString();





    }

    connect(m_ptcpServer, &QTcpServer::newConnection,this,&TcpServer::slotNewConnection);

    /*data helper*/

    bool isAndroid = false;
    dataHelper.setPlatfotm(isAndroid);


    //create lockation
    QDir mypath(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    mypath.mkpath("images");
    qDebug() << mypath.absolutePath();

    
}

ClientNamesModel *TcpServer::getPeerNamesModel()
{
    return &peerNamesModel;
}

TestResultModel *TcpServer::getResultModel()
{
    return &testResultModel;

}

void TcpServer::selectDevice(const QString &hostName)
{

    selectedClient = hostName;
}

void TcpServer::startTest(const QString &theme)
{
    qDebug() << "send start test" << theme;
    //currentTheme = theme;
    dataHelper.openTheme(theme);
    testResultModel.clearModel();
    for (int i = 0; i < dataHelper.getAllPicureNames().length(); i++)
    {
       testResultModel.addViewedPicture(dataHelper.getAllPicureNames().at(i),"0");
    }
    QTcpSocket *selectedSocket = clients.value(clientNames.key(selectedClient));
    if (selectedSocket != 0)
        sendCommandToClient(selectedSocket, SEND_THEME, theme);
    isTimerStarted = false;


}

void TcpServer::sendNextPicture(QTcpSocket* pClientSocket)
{
    qDebug() << "send next picture";
    if (dataHelper.hasNextPicture())
    {
        QString picture = dataHelper.getNextPicture();
        qDebug() << "sended picture" <<picture;
        sendCommandToClient(pClientSocket,SEND_PICTURE, picture);
    }
    else
    {
        qDebug() << "test end send";
        sendCommandToClient(pClientSocket,TEST_END);
    }
    isTimerStarted = false;

}

void TcpServer::startPictureTimer(int row)
{
    qDebug() << "start picture timer";
    pictureTimer.start();
    isTimerStarted = true;
}

QVariant TcpServer::getThemes()
{
    return dataHelper.getFolders();
}

void TcpServer::clientDisconnected()
{
    qDebug() << "client disconnected";
    QTcpSocket* pClient = static_cast<QTcpSocket*>(QObject::sender());

    qintptr savedDescriptor = clients.key(pClient);
    QString peerName = clientNames.value(savedDescriptor);
    qDebug() << peerName;
    peerNamesModel.removePeerName(peerName);
    clients.remove(savedDescriptor);
    clientNames.remove(savedDescriptor);
    emit sendClientDisconnected();
}

void TcpServer::testAborted()
{
    qDebug() << "test aborted";
    QTcpSocket *selectedSocket = clients.value(clientNames.key(selectedClient));
    if (selectedSocket != 0)
        sendCommandToClient(selectedSocket,TEST_END);

}

void TcpServer::slotNewConnection()
{
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection();
    QHostInfo hi = QHostInfo::fromName(pClientSocket->peerAddress().toString());
    QString hostName = hi.hostName();
    peerNamesModel.addPeerName(hostName, QUuid::createUuid().toString());
    clients.insert(pClientSocket->socketDescriptor(), pClientSocket);
    clientNames.insert(pClientSocket->socketDescriptor(), hostName);
    qDebug() << "new connection" << pClientSocket << hi.hostName() << pClientSocket->peerAddress().toString() << pClientSocket->socketDescriptor();;
    connect(pClientSocket, &QTcpSocket::disconnected, this, &TcpServer::clientDisconnected);
    connect(pClientSocket, &QTcpSocket::readyRead, this, &TcpServer::slotReadClient);
}

void TcpServer::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    qDebug() << "read socket" << pClientSocket << pClientSocket->bytesAvailable() ;
    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_5);
    for (;;)
    {
        if (m_nNextBlockSize == 0) {
            if (pClientSocket->bytesAvailable() < sizeof(quint64)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (pClientSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }
        quint8 request;
        in >> request;

        qDebug() << "socket request" << request;

        switch (request) {
        case READY_TO_VIEW:
        {
            qDebug() << "client ready to view";
            testResultModel.setData(testResultModel.index(0,0), "true", TestResultModel::StartVisible);
            testResultModel.setData(testResultModel.index(0,0), "green", TestResultModel::Color);

            sendNextPicture(pClientSocket);
        }
            break;        
        case PICTURE_VIEWED:
        {
            qDebug() << "client viewed picture";

            testResultModel.setData(testResultModel.index(dataHelper.getCurrentPicture(),0), "false", TestResultModel::StartVisible);
            testResultModel.setData(testResultModel.index(dataHelper.getCurrentPicture(),0), "white", TestResultModel::Color);
            qint64 time = 0;
            if (isTimerStarted)
                time = pictureTimer.elapsed();
            testResultModel.setData(testResultModel.index(dataHelper.getCurrentPicture(),0), QString::number(time), TestResultModel::TimeRole);

            testResultModel.setData(testResultModel.index(dataHelper.getCurrentPicture() + 1,0), "true", TestResultModel::StartVisible);
            testResultModel.setData(testResultModel.index(dataHelper.getCurrentPicture() + 1,0), "green", TestResultModel::Color);
            sendNextPicture(pClientSocket);

        }
            break;       
        default:
            break;
        }

        m_nNextBlockSize = 0;
    }


}

void TcpServer::sendCommandToClient(QTcpSocket *pSocket, quint8 type, QString data)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << type << data.toUtf8();
    out.device()->seek(0);
    out << quint64(arrBlock.size() - sizeof(quint64));
    qDebug() << "write" << arrBlock << data << pSocket->write(arrBlock);

    m_nNextBlockSize = 0;

}
