import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {


        Button {
            id: toTestButton
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: 360
            height: 40
            text: qsTr("Тест")

            onClicked: {
                stack.push(themeComponent)
                mode = 0
            }

        }

        Button {
            id: toResultButton
            anchors.top: toTestButton.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: 360
            height: 40
            text: qsTr("Вывод результата")

            onClicked: {                
                stack.push(themeComponent)
                mode = 1
            }

        }

        Button {
            id: backButton
            anchors.top: toResultButton.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: 360
            height: 40
            text: qsTr("Назад")
            onClicked:
            {
                stack.pop()
            }


        }

}
