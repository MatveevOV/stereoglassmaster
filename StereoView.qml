import QtQuick 2.9
import QtQuick.Dialogs 1.1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {

    property var lastTime: 0
    property int tryNumber: database.getUserTry(database.getCurrentUser(),dataHelper.getCurrentTheme())
    property bool testEnd: false


    Rectangle{
        id: leftRect
        width: parent.width/2-2
        height: parent.height
        x:0
        y:0
        color: "red"
        clip: true
        Image{
            id: leftPic

        }
    }

    Rectangle{
        id: rightRect
        width: parent.width/2-2
        height: parent.height
        x: parent.width/2+2
        y:0
        color: "blue"
        clip: true

        Image{

            id: rightPic

        }

    }


    Text {
        id: text
        anchors.centerIn: parent
        font.pointSize: 14
        text: qsTr("Нажмите для начала тестирования")
    }

    MouseArea
    {
        anchors.fill: parent
        onClicked: {

            setImage()
            text.visible = false

        }

    }


    Rectangle
    {
        id: messageRect
        anchors.centerIn: parent
        visible: false
        color: "white"
        width: 250
        height: 50

        Text{
            text: "Тестирование завершено"
            anchors.centerIn: parent
            font.pointSize: 14
        }


    }


    function setImage()
    {


        //сохранение прошлой картинки
        var time = new Date();
        var lastPicture =  dataHelper.getLastPicture()
        if (lastTime !== 0 && lastPicture !== "")
        {
            console.log(dataHelper.getLastPicture(), time.getTime() - lastTime)
            database.insertViewingTime(dataHelper.getCurrentTheme(),
                                       database.getCurrentUser(),
                                       lastPicture,
                                       tryNumber,
                                       time.getTime() - lastTime)

        }

        var leftPath
        var rightPath
        var prefix

        if (isAndroid)
        {
            prefix = ""
            leftPath = dataHelper.getLeftImage();
            rightPath = dataHelper.getRightImage();
        }
        else
        {
            prefix = filePrefix
            leftPath = prefix + dataHelper.getLeftImage();
            rightPath = prefix + dataHelper.getRightImage();
        }

        var picName = dataHelper.getPictureName()

        if (testEnd)
        {
            stack.pop()
            return
        }
        if (picName === "")
        {
            messageRect.visible = true
            leftPic.source = ""
            rightPic.source = ""
            testEnd = true

        }

        if (leftPath !== prefix && rightPath !== prefix )
        {
            leftPic.source = leftPath;
            rightPic.source = rightPath;

            leftPic.fillMode = Image.PreserveAspectFit
            rightPic.fillMode = Image.PreserveAspectFit
            leftPic.height = leftRect.height
            rightPic.height = rightRect.height
            leftPic.x = leftRect.width/2 - leftPic.width/2
            rightPic.x = rightRect.width/2 - rightPic.width/2


        }


        lastTime = new Date().getTime()


        }


    }



