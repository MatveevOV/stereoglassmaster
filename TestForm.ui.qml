import QtQuick 2.4

Item {
    id: item1
    width: 400
    height: 400

    Row {
        x: 0
        y: 19
        width: 400

        Text {
            id: text1
            width: 200
            text: qsTr("Text")
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 12
        }

        Text {
            id: text2
            text: qsTr("Text")
            font.pixelSize: 12
        }
    }
}
